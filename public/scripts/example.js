 

//评论盒子
var CommentBox = React.createClass({
  loadCommentsFromServer:function(){
      $.ajax({
        url:this.props.url,
        dataType:'json',
        success:function(data){
          this.setState({data:data});
        }.bind(this),
        error:function(xhr,status,err){
          console.error(this.props.url,status,err.toString());
        }.bind(this)
      })
  },
  handleCommentSubmit: function(comment) {
 
    var comments = this.state.data;
    
    comment.id = Date.now();
    var newComments = comments.concat([comment]);
    this.setState({data: newComments});

    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        this.setState({data: data});
      
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({data: comments});
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState:function(){
      return {data:[]}
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return  ( 

      < div className = "commentBox" >
      <h2>Comments</h2> 
        <CommentList data={this.state.data} /> 

        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div> 
      );
   
  }
});


 


//评论列表
var CommentList = React.createClass({
  render:function(){
    var commentNodes = this.props.data.map(function(comment){

        return (
          <Comment author={comment.author} key={comment.id} url={comment.url} time={comment.id} >
            {comment.text}
          </Comment>

        )
    });

    return (
        <div className="commentlist">
         {commentNodes}
        </div>
    );
  }
});


//评论框
var CommentForm = React.createClass({
  getInitialState: function() {
    return {author: '', text: '',url:''};
  },
  handleAuthorChange: function(e) {
    this.setState({author: e.target.value});
  },
  handleTextChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleUrlChange:function(e){
    this.setState({url:e.target.value})
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    var url = this.state.url.trim();

    if (!text || !author) {
      return;
    }
    this.props.onCommentSubmit({author: author, url: url, text: text});
    this.setState({author: '', url:'' ,text: ''});
  },
  render: function() {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="Your name"
          value={this.state.author}
          onChange={this.handleAuthorChange}
        /> 
        <input
          type="text"
          placeholder="your website"
          value={this.state.url}
          onChange={this.handleUrlChange}
        /> 
        <textarea
          type="text"
          placeholder="Say something..."
          value={this.state.text}
          onChange={this.handleTextChange}
        ></textarea>
        <input type="submit" value="Post" />
      </form>
    );
  }
});


//评论

var Comment = React.createClass({
  rawMarkup:function(){
    var rawMarkup = marked(this.props.children.toString(),{sanitize:true});
    return {__html:rawMarkup};
  },
  timeFormat:function(time){
    var date = new Date(1398250549490),
    Y = date.getFullYear() + '-',
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-',
    D = date.getDate() + ' ',
    h = date.getHours() + ':',
    m = date.getMinutes() + ':',
    s = date.getSeconds(); 
   return (Y+M+D+h+m+s); 
  },
  render:function(){
    return (
        <div className="comment">
          <div className="userinfo">
              <h1 className="headpic"> <img src="img/music.png" alt="" /></h1>
              <div className="username">
                    <a href={this.props.url}> <h2 className="commentAuthor">{this.props.author}</h2></a>
                    <h2 className="time">{ this.timeFormat(this.props.time)}</h2>
              </div>

          </div>
           
           <span dangerouslySetInnerHTML={this.rawMarkup()} />
        </div>



    )
  }
});

ReactDOM.render( < CommentBox url="/api/comments" pollInterval={2000} / > ,
  document.getElementById('content')
);


 

